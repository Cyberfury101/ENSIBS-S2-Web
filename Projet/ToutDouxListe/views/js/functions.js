async function ajout(nom){ // Fonction d'ajout de tache

    if (nom === ""){ // Si le nom est vide
        console.log("Nom vide"); // On affiche alors dans la console "Nom vide"
        return 0; // On retourne 0
    } else {

        await fetch('/tasks', { // Requête ajax sur l'endpoint /tasks
            headers: { // Définition de nos headers
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post', // On utilise le verbe POST
            body: JSON.stringify( { "libelle":nom } ) // Envoie dans le body un JSON libelle avec notre paramètre
        }).then(
            console.log(nom + " créé !") // On affiche alors dans la console 
        );

        location.reload(true) // On reload automatiquement notre page pour que nos 
    
    }

};

async function supprimer(id){ // Fonction de suppresion de tache

    await fetch('/tasks', { // Requête ajax sur l'endpoint /tasks
        headers: { // Définition de nos headers
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'delete', // On utilise le verbe DELETE
        body: JSON.stringify( { "id":id } ) // Envoie dans le body un JSON id avec notre paramètre
    }).then(
        console.log(id + " supprimé !") // On affiche dans la console que tout s'est bien déroulé
    );

    location.reload(true)

};

async function modifier(status,id){ // Fonction de modification de tache

    let newStatus = "A_FAIRE"; // On définie une variable newStatus à "A_FAIRE"

    if (status === "A_FAIRE"){ // Si le paramètre donné à la fonction est déjà à "A_FAIRE"
        newStatus = "FAIT"; // Alors notre newStatus devient alors "FAIT"
    }

    await fetch('/tasks', { // Requête ajax sur l'endpoint /tasks
        headers: { // Définition de nos headers
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'put', // On utilise le verbe PUT
        body: JSON.stringify( { "status":newStatus,"id":id } ) // Envoie dans le body un JSON status et id avec nos paramètres
    }).then(
        console.log(status + " devient : " + newStatus)
    ).then(response => 
        console.log(response) // On affiche la réponse dans la console
    );

    location.reload(true)
    
};

async function recherche(nom){ // Fonction de recherche de tache
    
    $('.collapse').collapse('show') // On affiche nos collapse

    await fetch('/tasks/'+nom, { // Requête ajax sur l'endpoint /tasks/<nom de la recherche>
        headers: { // Définition de nos headers
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'get' // On utilise le verbe GET
    }).then( response =>
        response.json() // On transforme notre réponse en json
    ).then( message => {
        console.log(message); // On affiche le message dans la console
        document.getElementById('resNom').innerText = message.message[0].libelle; // On remplace le text compris dans l'id "resNom" par notre libelle
		document.getElementById('resStatus').innerText = message.message[0].status; // On remplace le text compris dans l'id "resStatus" par notre status
    }). catch(error => {
        console.log(error); // Si il y a une erreur on l'affiche dans la console
    });

};