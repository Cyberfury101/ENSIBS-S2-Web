const express = require("express"); // On importe le module express
const bodyParser = require("body-parser"); // On importe le module body-parser
const path = require('path'); // On importe le module path
const db = require("./db.js"); //On importe le fichier db.js avec les paramètres de connexion

const app = express();

// use : Utilisation des Middlewares Express
app.use(bodyParser.json()); // Parse les requêtes avec content-type: application/json
app.use(bodyParser.urlencoded({ extended: false })); // Parse les requêtes avec content-type: application/x-www-form-urlencoded

// Route racine appelé par défaut
app.get("/", (req, res) => {
    db.then( pool =>
        pool.query('SELECT id,libelle,status from taches') // On exécute la requête qui va récupérer id, libelle et status notre table "taches"
    ).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
        res.render('index.ejs', {todolist: results}); // Appel la page index.ejs avec comme corp un objet todolist en json avec les résultats de la quête sql
    });
});

// Route /contact
app.get("/contact", (req, res) => {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/contact.html'));	// Renvoie le fichier contenu dans /views/contact.html
});

// Route /aide
app.get("/aide", (req, res) => {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/aide.html')); // Renvoie le fichier contenu dans /views/aide.html
});

// Route /tasks/<nom de la recherche>
app.get("/tasks/:recherche", (req, res) => {
	db.then( pool => 
		pool.query('SELECT id,libelle,status from taches WHERE libelle = ?', [req.params.recherche]) // On sélectionne toutes les entées de la table "taches" quand la valeur du champ libelle est la même que celle donné en paramètre
	).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
		res.location("/tasks/"); // Renvoie l'header location : /tasks/
		res.json({ message: results }); // On retourne alors un objet json message avec les résultats de la requête sql
	});
});

// Route /tasks
app.get("/tasks/", (req, res) => {
    db.then( pool =>
        pool.query('SELECT id,libelle,status from taches') // On exécute la requête qui va récupérer id, libelle et status notre table "taches"
    ).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
		res.location("/tasks/"); // Renvoie l'header location : /tasks/
        res.json({ message: results }); // On retourne alors un objet json message avec les résultats de la requête sql
    });
});

// Route /tasks avec le verbe PUT
app.put("/tasks/", (req, res) => {
	db.then( pool => 
		pool.query('UPDATE taches SET status=? WHERE id=?', [req.body.status,req.body.id]) // On modifie le champ status de la table "taches" avec la chaîne de caractère donnée en paramètre
	).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
		res.location("/tasks/"); // Renvoie l'header location : /tasks/
		res.json({ message: results }); // On retourne alors un objet json message avec les résultats de la requête sql
	});
});

// Route /tasks avec le verbe PUT
app.post("/tasks/", (req, res) => {
	db.then( pool => 
		pool.query('INSERT INTO taches(libelle,status) VALUES(?,"A_FAIRE")', [req.body.libelle]) // On insert dans la table "taches" les champs libelle et status. On n'ajoute pas d'id car il s'auto incrémente
	).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
		res.location("/tasks/") // Renvoie l'header location : /tasks/
		res.json({ message: results }); // On retourne alors un objet json message avec les résultats de la requête sql
	});
});

// Route /tasks avec le verbe DELETE
app.delete("/tasks/", (req, res) => {
	db.then( pool => 
		pool.query('DELETE from taches WHERE id=?', [req.body.id]) // On supprime de la BDD la ligne de la table "taches" dont l'id est celui donné en paramètre
	).then( results => {
		res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
		res.location("/tasks/") // Renvoie l'header location : /tasks/
		res.json({ message: results }); // On retourne alors un objet json message avec les résultats de la requête sql
	});
});

// Route /images/logo.png
app.get('/images/logo.png', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/images/logo.png')); // Renvoie le fichier contenu dans /views/images/logo.png
});

// Route /images/del.png
app.get('/images/del.png', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/images/del.png')); // Renvoie le fichier contenu dans /views/images/del.png
});

// Route /images/edit.png
app.get('/images/edit.png', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/images/edit.png')); // Renvoie le fichier contenu dans /views/images/edit.png
});

// Route /font/SciFly-Sans.ttf
app.get('/font/SciFly-Sans.ttf', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/font/SciFly-Sans.ttf')); // Renvoie le fichier contenu dans /views/images/SciFly-Sans.ttf
});

// Route /style/index.css
app.get('/style/index.css', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/style/index.css')); // Renvoie le fichier contenu dans /views/images/index.css
});

// Route /js/functions.js
app.get('/js/functions.js', function (req, res) {
	res.status(201); // Renvoie le status 201 disant que tout s'est bien déroulé
    res.sendFile(path.join(__dirname, '/views/js/functions.js')); // Renvoie le fichier contenu dans /views/images/functions.js
});

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});
