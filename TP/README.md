# ENSIBS-S2-Web-TP

Module d'introduction au Web.<br/>
Ce module fait partie du cursus Cyberdéfense par apprentissage de l'ENSIBS à Vannes.<br/>
Cours présenté par Frédéric Linot.<br/>

TP réalisé par xThaz.

# Utilisation

Une fois l'archive décompilée, il suffit d'ajouter sa clé d'API dans le fichier "index.html" à la ligne 93 tel que :

    url: 'http://api.openweathermap.org/data/2.5/weather?q=' + inputVille.value + ',fr&APPID=[REDACTED]

Devienne :

    url: 'http://api.openweathermap.org/data/2.5/weather?q=' + inputVille.value + ',fr&APPID=FRXG46X8RDFT8XDR31T42DRXVCT6XDR1V4BXD8R69Y4DX16

Ensuite il suffit de lancer le fichier `index.html` avec votre navigateur. </br>
Vous voila sur notre site !

Pour naviguer dans le site, vous pouvez soit cliquer sur une des pages en haut (Accueil, Contact ou Aide) ou alors dans la page d'Acc

# Création d’une application web de météo.

## Contexte
Cette semaine vous allez devoir créer un site Web affichant la météo du jour concernant une ville donnée.

## Consignes
Créez un site Web affichant les prévisions météo d’une ville. Le nom de cette ville aura été renseigné par l’utilisateur. Votre site Web devra donc comporter au minimum :
- un champ de texte pour saisir la ville
- un bouton pour valider la ville saisie
- différente partie (div/table, etc..) affichant la prévision
- un menu de navigation (accueil, contact, aide)
- un pied de page

Autres restrictions :
- Les navigateurs les plus connus devront pouvoir afficher votre site Web.
- Le site devra être «responsive» pour un affichage sur ordinateur, mais aussi sur plate-forme mobile.
- Il ne devra y avoir aucun rechargement de page lors de la prise en compte de la ville saisiepar l’utilisateur.
- Lors du clic sur les informations concises, des informations plus précises s’affichent.

# Dépôt

- Code de l’application + un fichier Readme.txt (du type Getting Started). L’archive doit impérativement être au format ZIP, avec la nomenclature suivante : NomPrenomNumeroDeGroupeSpecialité
- Le code de l’application devra être entièrement commenté en français. Vous trouverez une documentation sur « Bien commenter son code » à cette adresse : https://buzut.net/bien-commenter-son-code/
- Un rapport détaillant les blocs de code important commentés correspondant aux fonctionnalités. Le rapport devra également contenir des captures d’écran de votre application (i.e. : la page d’accueil, le résultat de la requête) ainsi qu'une courte vidéo (30s suffisent) montrant comment votre application réagit aux actions utilisateur.
